import socket
import select
import sys
import json

TCP_IP = socket.gethostbyname(socket.gethostname())
TCP_PORT = 5000
BUFFER_SIZE = 1024

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((TCP_IP, TCP_PORT))
server.listen(5)

connections = []
connections.append(server)
users = {}
socketIP = {}

print("Server started on " + TCP_IP)

while True:
    # Check if there are any readable sockets
    try:
        readable_sockets, writeable_sockets, error_sockets = select.select(connections, [], [])
    except select.error:
        print("Server killed")
        sys.exit(0)

    for s in readable_sockets:
        if s == server:
            connection, address = server.accept()
            print("Connection from " + str(address))
            socketIP[connection] = address[0]
            connections.append(connection)
        else:
            try:
                received_data = s.recv(BUFFER_SIZE)
                if received_data.startswith(b"REMOVE:"):
                    try:
                        del users[received_data[7:].decode()]
                    except KeyError:
                        pass
                else:
                    if b"GET" not in received_data:
                        users[received_data.decode()] = socketIP[s]
                    json.dump(users, s)
            except (socket.error, json.JSONDecodeError):
                print("Error receiving data")
                connections.remove(s)
                s.close()
