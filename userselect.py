from PySide2 import QtCore, QtWidgets
from nameclient import NameClient
from chat import Chat
from chatserver import ChatServer
from message import Message


class UserSelect(QtWidgets.QDialog):

    def __init__(self, username, ciphers):
        super().__init__()
        self.username = username
        self.ciphers = ciphers
        self.userList = {}

        self.chatserver = ChatServer(self.ciphers)
        self.chatserver.update.connect(self.receivedMessage)
        self.chatserver.start()

        self.chats = {}
        self.setWindowTitle("Users")
        self.resize(300, 400)

        self.layout = QtWidgets.QGridLayout(self)

        # listWidget
        self.list = QtWidgets.QListWidget(self)
        self.userList = self.getUsers()
        users = list(self.userList)
        self.list.addItems(users)
        self.layout.addWidget(self.list, 0, 0, 1, 3)

        # select button
        self.selectbutton = QtWidgets.QPushButton("Select", self)
        self.selectbutton.setMinimumWidth(20)
        self.selectbutton.setMinimumHeight(50)
        self.layout.addWidget(self.selectbutton, 1, 0)
        self.selectbutton.clicked.connect(self.chatStarted)

        # refresh button
        self.refreshbutton = QtWidgets.QPushButton("Refresh", self)
        self.refreshbutton.setMinimumWidth(20)
        self.refreshbutton.setMinimumHeight(50)
        self.layout.addWidget(self.refreshbutton, 1, 1)
        self.refreshbutton.clicked.connect(self.refreshUsers)

        # logout button
        self.logoutbutton = QtWidgets.QPushButton("Log out", self)
        self.logoutbutton.setMinimumWidth(5)
        self.logoutbutton.setMinimumHeight(50)
        self.layout.addWidget(self.logoutbutton, 1, 2)
        self.logoutbutton.clicked.connect(self.quitProgram)

        self.setLayout(self.layout)
        self.show()

    def chatStarted(self):
        clickedItem = self.list.currentItem()
        if clickedItem is not None:
            user = str(clickedItem.text())
            ip = self.userList[user]
            if ip is not None:
                chat = Chat(self.username, user, ip, self.ciphers)
                self.chats[user] = chat

    def receivedMessage(self, data):
        message = Message()
        message.fromJson(str(data))
        user = message.getUser()
        if user in self.chats:
            chat = self.chats[user]
            chat.receiveMessage(message)
        else:
            self.refreshUsers()
            ip = self.userList[user]
            chat = Chat(self.username, user, ip, self.ciphers)
            self.chats[user] = chat
            chat.receiveMessage(message)

    def getUsers(self):
        client = NameClient()
        if client.connect():
            client.sendUsername(self.username)
            return client.receiveUserList()

        return {'Server error': None}

    def refreshUsers(self):
        self.userList = self.getUsers()
        users = list(self.userList)
        self.list.clear()
        self.list.addItems(users)

    def logOut(self):
        client = NameClient()
        if client.connect():
            client.sendUsername(f"REMOVE:{self.username}")

    def quitProgram(self):
        self.logOut()
        self.chatserver.end()
