from PySide2 import QtWidgets, QtWidgets


class Error(QtWidgets.QDialog):

    def __init__(self, message):
        super().__init__()
        self.setWindowTitle("Error")
        self.resize(500, 100)
        self.layout = QtWidgets.QGridLayout(self)
        error_label = QtWidgets.QLabel(message)
        self.layout.addWidget(error_label, 0, 0)
        self.setLayout(self.layout)
        self.show()
