from userpref import UserPref
from PySide2 import QtWidgets
import sys


app = QtWidgets.QApplication(sys.argv)
usernameWindow = UserPref()
usernameWindow.show()

sys.exit(app.exec_())
