import socket
import select
import sys
import ssl
import os.path
from PySide2 import QtCore
from OpenSSL import crypto


class ChatServer(QtCore.QThread):
    update = QtCore.Signal(str)

    def __init__(self, ciphers):
        super().__init__()
        self.ciphers = ciphers
        self.IP = socket.getaddrinfo(socket.gethostname(), None)[0][4][0]
        self.TCP_PORT = 5005
        self.BUFFER_SIZE = 1024
        self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.running = False
        self.serversocket.bind((self.IP, self.TCP_PORT))
        self.serversocket.listen(5)

        self.generate_certificate()

        self.connections = []
        self.connections.append(self.serversocket)

    def generate_certificate(self):
        certificate_file_name = "cert.pem"
        key_file_name = "key.key"

        if os.path.isfile(certificate_file_name) and os.path.isfile(key_file_name):
            return
        else:
            key = crypto.PKey()
            key.generate_key(crypto.TYPE_RSA, 1024)

            certificate = crypto.X509()

            certificate.get_subject().C = "US"
            certificate.get_subject().ST = "CA"
            certificate.get_subject().L = "IV"
            certificate.get_subject().O = "UCSB"
            certificate.get_subject().OU = "UCSB"
            certificate.get_subject().CN = self.IP

            certificate.gmtime_adj_notBefore(0)
            certificate.gmtime_adj_notAfter(365 * 24 * 60 * 60)

            certificate.set_issuer(certificate.get_subject())
            certificate.set_pubkey(key)
            certificate.sign(key, 'sha1')

            with open(certificate_file_name, "wt", encoding='utf-8') as certificate_file:
                certificate_file.write(crypto.dump_certificate(crypto.FILETYPE_PEM, certificate))

            with open(key_file_name, "wt", encoding='utf-8') as key_file:
                key_file.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, key))
    def run(self):
        self.running = True
        print("Running server")
        while self.running:
            try:
                readable_sockets, _, _ = select.select(self.connections, [], [])
            except select.error:
                break
            for s in readable_sockets:
                if s == self.serversocket:
                    connection, _ = self.serversocket.accept()
                    sslconnection = ssl.wrap_socket(connection,
                                                    server_side=True,
                                                    certfile="cert.pem",
                                                    keyfile="key.key",
                                                    ssl_version=ssl.PROTOCOL_TLSv1_2,
                                                    ciphers=self.ciphers)
                    self.connections.append(sslconnection)
                    print("Connected using: " + str(sslconnection.cipher()))

                else:
                    try:
                        data = s.recv(self.BUFFER_SIZE)
                        if data == "":
                            s.close()
                            self.connections.remove(s)
                        else:
                            self.update.emit(data)
                    except socket.error:
                        print("error data...")
                        break

    def end(self):
        self.running = False
        print("Server shutting down...")
        for conn in self.connections:
            conn.close()
        self.serversocket.close()
        sys.exit(0)
