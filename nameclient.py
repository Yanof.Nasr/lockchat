import socket
import json


class NameClient():

    def __init__(self):
        self.server = None
        self.CLIENT_TCP_IP = socket.gethostbyname(socket.gethostname())
        self.SERVER_TCP_IP = '127.0.1.1'
        self.SERVER_TCP_PORT = 5000
        self.BUFFER_SIZE = 1024

    def connect(self):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.server.connect((self.SERVER_TCP_IP, self.SERVER_TCP_PORT))
            self.server.settimeout(3)
            return True
        except socket.error:
            return False
        
    def sendUsername(self, username):
        try:
            self.server.send(username)
        except socket.error:
            print("Could not send message, disconnecting")

    def receiveUserList(self):
        userList = {}
        try:
            data = self.server.recv(self.BUFFER_SIZE)
            self.server.close()
            try:
                userList = json.loads(data.decode())
            except ValueError:
                #bad data
                pass
        except socket.timeout:
            #timed out
            pass
        return userList
