from PySide2 import QtCore, QtWidgets
from userselect import UserSelect
from error import Error


class UserPref(QtWidgets.QDialog):
    """A dialog window for setting user preferences."""

    def __init__(self):
        """Initialize the UserPref dialog."""
        super(UserPref, self).__init__()
        self.setWindowTitle("Preferences")
        self.resize(500, 50)

        self.layout = QtWidgets.QGridLayout(self)

        # Name input
        self.name_input = QtWidgets.QLineEdit('', self)
        self.name_input.setStyleSheet("font: 20pt")
        self.name_input.setFixedHeight(30)
        self.layout.addWidget(self.name_input, 1, 0)
        user_label = QtWidgets.QLabel('Username:')
        self.layout.addWidget(user_label, 0, 0)

        # OK button
        self.ok_button = QtWidgets.QPushButton(self)
        self.ok_button.setText("OK")
        self.ok_button.setMinimumWidth(50)
        self.ok_button.setMinimumHeight(45)
        self.layout.addWidget(self.ok_button, 1, 1)
        self.ok_button.clicked.connect(self.log_on)

        # Crypto group
        self.crypto_boxes = []
        ciphers = ["CAMELLIA256-SHA", "AES256-SHA", "AES128-SHA", "DES-CBC3-SHA", "RC4-SHA"]

        self.crypto_group = QtWidgets.QGroupBox("Ciphers", self)
        self.crypto_group.setStyleSheet("font: 13pt")
        self.crypto_group_layout = QtWidgets.QGridLayout()
        all_box = QtWidgets.QCheckBox("ALL")
        all_box.setChecked(True)
        self.crypto_boxes.append(all_box)
        self.crypto_group_layout.addWidget(all_box, 0, 0)
        high_box = QtWidgets.QCheckBox("HIGH")
        self.crypto_boxes.append(high_box)
        self.crypto_group_layout.addWidget(high_box, 1, 0)
        medium_box = QtWidgets.QCheckBox("MEDIUM")
        self.crypto_boxes.append(medium_box)
        self.crypto_group_layout.addWidget(medium_box, 2, 0)
        low_box = QtWidgets.QCheckBox("LOW")
        self.crypto_boxes.append(low_box)
        self.crypto_group_layout.addWidget(low_box, 3, 0)
        for cipher in ciphers:
            cipher_box = QtWidgets.QCheckBox(cipher)
            self.crypto_boxes.append(cipher_box)
            self.crypto_group_layout.addWidget(cipher_box, ciphers.index(cipher), 1)

        self.crypto_group.setLayout(self.crypto_group_layout)
        self.layout.addWidget(self.crypto_group, 2, 0, 1, 2)

        self.setLayout(self.layout)

    def log_on(self):
        """Log the user in and open the user select window."""
        username = str(self.name_input.text())
        ciphers_list = ""
        for check_box in self.crypto_boxes:
            if check_box.isChecked():
                name = str(check_box.text())
                ciphers_list += (name + ":")
        ciphers_list = ciphers_list.strip(':')
        if username != "" and username[0:7] != "REMOVE:" and ciphers_list:
            self.users_window = UserSelect(username, ciphers_list)
            self.accept()
        else:
            self.error = Error("Username and cipher must be set.")
