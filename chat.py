import socket
import ssl
import errno
from PySide2 import QtCore, QtWidgets
from message import Message
from error import Error


class Chat(QtWidgets.QDialog):
    def __init__(self, username, contact, ip, ciphers):
        super(Chat, self).__init__()
        self.chatLog = []
        self.username = username
        self.contact = contact
        self.ciphers = ciphers
        self.setWindowTitle("Chat with " + str(contact))
        self.resize(600, 400)

        if not self.connect(ip):
            self.accept()
            return

        # Text
        self.scrollArea = QtWidgets.QScrollArea(self)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.chatText = QtWidgets.QTextBrowser(self.scrollAreaWidgetContents)

        # SendButton
        self.sendButton = QtWidgets.QPushButton("Send", self)
        self.sendButton.setMinimumWidth(50)
        self.sendButton.setMinimumHeight(45)
        self.sendButton.clicked.connect(self.clickedButton)

        # messageText
        self.messageText = QtWidgets.QLineEdit(self)

        # Layout
        layout = QtWidgets.QVBoxLayout(self)
        layout.addWidget(self.scrollArea)
        layout.addWidget(self.messageText)
        layout.addWidget(self.sendButton)

        self.setLayout(layout)
        self.refreshChatMessages()
        self.show()

    def receiveMessage(self, message):
        self.chatLog.append(message)
        self.refreshChatMessages()

    def sendMessage(self, message):
        self.chatLog.append(message)
        self.refreshChatMessages()
        try:
            self.sslSocket.send(message.toJson())
        except ssl.SSLError as e:
            self.error = Error(str(e))
        except socket.error as e:
            errorcode = e[0]
            if errorcode == errno.EPIPE:
                self.error = Error("Could not send message, user has probably logged out")
                try:
                    self.sslSocket.close()
                except:
                    None

    def connect(self, ip):
        self.port = 5005
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sslSocket = ssl.wrap_socket(client,
                                          ssl_version=ssl.PROTOCOL_TLSv1_2,
                                          ciphers=self.ciphers)
        self.sslSocket.settimeout(3)
        try:
            self.sslSocket.connect((ip, self.port))
            return True
        except socket.error as e:
            errorcode = e[0]
            if errorcode == errno.ECONNREFUSED:
                self.error = Error("Could not connect to " + self.contact)
            else:
                messageError = "Error code " + str(e[0]) + "\nYour cipher preferences might be incompatible."
                self.error = Error(messageError)
            return False

    def clickedButton(self):
        text = self.messageText.text()
        message = Message(self.username, text)
        self.sendMessage(message)

    def refreshChatMessages(self):
        html = ""
        self.messageText.clear()
        for post in self.chatLog:
            if post.getUser() == self.username:
                html += '<div align="right">'
                html += post.getMessage()
                html += " ("
                html += post.getTime()
                html += ")"
            else:
                html += "<div>"
                html += "("
                html += post.getTime()
                html += ") "
                html += post.getMessage()
            html += "</div>"
        self.chatText.setHtml(html)
